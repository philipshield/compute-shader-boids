﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Linq;

public class Timer : MonoBehaviour {
	public string logDir;
	public string testName;
	private bool running;
	private StreamWriter writer;
	List<float> frameTimes;


	// Use this for initialization
	void Start () {
		running = false;
		logDir = "logs";
		frameTimes = new List<float> ();
		if (!Directory.Exists (logDir)) {
			Directory.CreateDirectory(logDir);
		}
	}

	public void StartTimer(string testName) {
		this.testName = testName;
		running = true;
	}

	public void EndTimer() {
		if (!running) {
			return;
		}

		running = false;
		using (writer = File.CreateText (Path.Combine(logDir, testName+".log"))) {
			foreach (float dt in frameTimes) {
				writer.WriteLine(dt);
			}

			WriteStatistics();
			frameTimes.Clear();
		}

	}

	private void WriteStatistics() {
		writer.WriteLine ("#Average, Max, Min, Frames, Standard Deviation, Median");
		var avg = frameTimes.Average ();
		writer.WriteLine (avg);
		writer.WriteLine (frameTimes.Max ());
		writer.WriteLine (frameTimes.Min ());
		writer.WriteLine (frameTimes.Count ());
		writer.WriteLine (Mathf.Sqrt(frameTimes.Average(v=>Mathf.Pow(v-avg,2))));
		writer.WriteLine (Median (frameTimes));
	}

	private float Median(List<float> xs) {
		var ys = xs.OrderBy(x => x).ToList();
		double mid = (ys.Count - 1) / 2.0;
		return (ys[(int)(mid)] + ys[(int)(mid + 0.5)]) / 2;
	}



	// Update is called once per frame
	void Update () {
		if (!running) {
			return;
		}
		frameTimes.Add (Time.unscaledDeltaTime);
	}
}
