﻿Shader "DX11/PlaneBufferShader"
{
	Properties {
		_Color("color", Color) = (1.0, 1.0, 1.0, 1.0)
	}
	SubShader
	{
		Pass
		{
		CGPROGRAM
#pragma target 5.0

#pragma vertex vert
#pragma fragment frag
		

#include "UnityCG.cginc"

		//The buffer containing the points we want to draw.
		StructuredBuffer<float3> positions;
		//StructuredBuffer<float3> velocities;

		uniform float4 _Color;

			struct VS_IN {
				uint id : SV_VertexID;
			};

			struct VS_OUT {
				float4 pos : SV_POSITION;
				//float4 vel : COLOR;
			};

			// VERTEX SHADER
			VS_OUT vert(VS_IN i)
			{
				VS_OUT o;

				float3 worldPos = positions[i.id];
				o.pos = mul(UNITY_MATRIX_VP, float4(worldPos, 1.0f));
				//o.vel.xyz = velocities[i.id];

				return o;
			}

			// FRAGMENT SHADER
			float4 frag(VS_OUT i) : COLOR
			{
				return _Color;
				//float speed = length(i.vel);
				//return saturate(float4(speed, .2f, .1f, 1)*speed);
			}

			ENDCG

		}
	}

	Fallback Off
}