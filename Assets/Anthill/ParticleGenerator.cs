﻿//ALGORITHM:
#define COMPUTE_SHADER

// Includes
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

//This game object invokes PlaneComputeShader (when attached via drag'n drop in the editor) using the PlaneBufferShader (also attached in the editor)
//to display a grid of points moving back and forth along the z axis.
public class ParticleGenerator : MonoBehaviour
{
    // Which Implementation
    public bool USE_COMPUTE_SHADER;

    // Compute Shader 
    public ComputeShader computeShader;
    private int group_size_x = 32;
    private int group_size_y = 32;
    private int group_size_z = 1;
    private int thread_group_size_x = 4;
    private int thread_group_size_y = 4;
    private int thread_group_size_z = 1;
    private int _kernel;

    // Path
    public Transform[] Path;
    private LineRenderer DrawablePathLineRenderer;

    // Particles
    public Shader particleShader;
    public Color ParticleColor;
    private Material material;
    
    // Boid Behaviour
    [Range(1, 25000)] public int N = 16384;
    [Range(0.01f, 9f)] public float MaxSpeed;
    [Range(0, 1f)] public float MaxForce;
    public Transform TargetTransform;
    [Range(0, 2)] private float ArriveDist;
    [Range(0, 10)] private float Mass = 1;
    [Range(0, 2)] public float Seek; 
    [Range(0, 3f)] public float VisionRadius;
    [Range(0, 2)] public float Separate;
    [Range(0, 2)] public float Align;
    [Range(0, 2)] public float Cohese;

    // boid data for compute shader
    private int i_current;
    private int i_last;
    private ComputeBuffer[] positionBuffers_GPU;
    private ComputeBuffer[] velocityBuffers_GPU;

    private ComputeBuffer pathBuffer;
    private ComputeBuffer cBuffer;
    private ComputeBuffer cIntBuffer;

    // boid data on cpu
    private float[] positionarray;
    private Vector3[,] positionBuffers_CPU;
    private Vector3[,] velocityBuffers_CPU;

	// PerformanceTest
	private bool hasBuffers = false;
	public bool runPerformanceTest;
	public Timer timeLogger;

	private IEnumerator Tests() {
		Debug.Log ("Starting test in 2 seconds");
		yield return new WaitForSeconds(2f);
		N = 500;
		for (int i=0; i<10; ++i)
		{
			N+=500;
			Debug.Log("Init test with: "+N+" particles running on: "+(USE_COMPUTE_SHADER?"GPU":"CPU"));
			timeLogger.StartTimer((USE_COMPUTE_SHADER?"GPU":"CPU")+"_"+N);
			InitGenerator();
			yield return new WaitForSeconds(20f);
			timeLogger.EndTimer();
		}
	}

    private int[] calculateGroupSize(int N, int x, int y, int z)
    {
        int groupsize = (int)Mathf.Ceil((float)N/(x*y*z));

        //fix gy to 32
        int newgy = 32;
        int neededGx = (int)Mathf.Ceil((float)groupsize / newgy);

        int neededGy = newgy;
        if(neededGx > 1024)
        {
            int newgx = 1024;
            neededGx = newgx;
            neededGy = (int)Mathf.Ceil((float)groupsize / newgx);
        }

        return new[] { neededGx, neededGy };
    }

    //We initialize the buffers and the material used to draw.
    void Start()
	{
		if (runPerformanceTest)
			StartCoroutine (Tests ());
		else
			InitGenerator();

        //Fix Path
        DrawablePathLineRenderer = gameObject.GetComponent<LineRenderer>() != null 
            ? gameObject.GetComponent<LineRenderer>()
            : gameObject.AddComponent<LineRenderer>();
        DrawablePathLineRenderer.material = new Material(Shader.Find("Particles/Additive"));
        DrawablePathLineRenderer.SetColors(Color.blue, Color.blue);
        DrawablePathLineRenderer.SetVertexCount(Path.Length);
        for (int i = 0; i < Path.Length; i++)
        {
            DrawablePathLineRenderer.SetPosition(i, Path[i].position);
            DrawablePathLineRenderer.SetWidth(Path[i].localScale.x, 0.1f);
        }

        _kernel = computeShader.FindKernel("CSMain");
    }

	private void InitGenerator()
	{
		if (hasBuffers)
		{
			ReleaseBuffers();
		}
		else
		{
			hasBuffers = true;
		}

		material = new Material(particleShader);
		
		//calculate group sizes
		var groupSize = calculateGroupSize(N, thread_group_size_x, thread_group_size_y, thread_group_size_z);
		group_size_x = groupSize[0];
		group_size_y = groupSize[1];
		
		CreateBuffers();
	}
    

    //When this GameObject is disabled we must release the buffers or else Unity complains.
    private void OnDisable()
    {
        ReleaseBuffers();
    }

    //After all rendering is complete we dispatch the compute shader and then set the material before drawing with DrawProcedural
    //this just draws the "mesh" as a set of points
    void OnPostRender()
    {
		if (!hasBuffers)
			return;
        //simulate and swap
        if (USE_COMPUTE_SHADER) 
        {
            Simulate_GPU();
        }
        else
        {
            Simulate_CPU();
        }

        material.SetPass(0);
        material.SetBuffer("positions", positionBuffers_GPU[i_current]);
        material.SetColor("_Color", ParticleColor);
        //material.SetBuffer("velocities", velocityBuffers_GPU[swap]);
        Graphics.DrawProcedural(MeshTopology.Points, N);

        swap();
    }

    void swap()
    {
        if(i_last == 1)
        {
            i_last = 0;
            i_current = 1;
        }
        else
        {
            i_last = 1;
            i_current = 0;
        }
    }

    //To setup a ComputeBuffer we pass in the array length, as well as the size in bytes of a single element.
    //We fill the offset buffer with random numbers between 0 and 2*PI.
    void CreateBuffers()
    {

        // current swap
        i_last = 0;
        i_current = 1;

        // Create Constants Buffer
        // - float t
        // - float dt
        // - float maxForce;
        // - float maxSpeed;
        // - float arriveDist;
        // - float mass;
        // - float3 target;
        // - float seek
        // - float visionradius
        // - float separate
        // - float align
        // - float cohese
        cBuffer = new ComputeBuffer(1, sizeof(float)*14);

        // Create 
        // - group_size_x
        // - N
        cIntBuffer = new ComputeBuffer(1, sizeof(int) * 2);

        // Create Path Buffer
        pathBuffer = new ComputeBuffer(Path.Length, sizeof(float)*3);

        //Create position & velocity Buffers
        positionBuffers_CPU = new Vector3[2, N];
        positionBuffers_GPU = new ComputeBuffer[2];
        positionBuffers_GPU[0] = new ComputeBuffer(N, sizeof(float)*3);
        positionBuffers_GPU[1] = new ComputeBuffer(N, sizeof(float)*3);

        velocityBuffers_CPU = new Vector3[2, N];
        velocityBuffers_GPU = new ComputeBuffer[2];
        velocityBuffers_GPU[0] = new ComputeBuffer(N, sizeof(float)*3);
        velocityBuffers_GPU[1] = new ComputeBuffer(N, sizeof(float)*3);

        // Initial Positions
        //TODO: create helperclass to generate starting positions along surfaces etc
        float planesize = 40;
        float offsetxy = planesize / 2;
        positionarray = new float[N * 3];
        int row = (int)Mathf.Ceil(Mathf.Sqrt(N));
        float step = planesize / row;
        for (int i = 0, j = 0; i < N*3; i+=3, ++j)
        {
            //float x = Random.value * planesize - offsetxy;
            //float y = Random.value < 0.5 ? -20 : -10;
            //float z = Random.value * planesize - offsetxy;

            float x = (j % row) * step - offsetxy;
            float y = 1;
            float z = (j / row) * step - offsetxy;

            positionarray[i + 0] = x;
            positionarray[i + 1] = y;
            positionarray[i + 2] = z;
            positionBuffers_CPU[i_last, j].x = x;
            positionBuffers_CPU[i_last, j].y = y;
            positionBuffers_CPU[i_last, j].z = z;
        }     
        
        positionBuffers_GPU[i_last].SetData(positionarray);
        velocityBuffers_GPU[i_last].SetData(new float[N * 3]); //velocities <0,0,0>

    }

    //Remember to release buffers and destroy the material when play has been stopped.
    void ReleaseBuffers()
    {
        positionBuffers_GPU[0].Release();
        positionBuffers_GPU[1].Release();
        velocityBuffers_GPU[0].Release();
        velocityBuffers_GPU[1].Release();
        
        pathBuffer.Release();
        cBuffer.Release();
        cIntBuffer.Release();
        
        DestroyImmediate(material);
    }

    // The meat of this script, it sets the constant buffer (current time) and then sets all of the buffers for the compute shader.
    // We then dispatch 32x32x1 groups of threads of our CSMain kernel.
    void Simulate_GPU()
    {
        // set constant float buffers
        cBuffer.SetData(new float[] { Time.time, Time.deltaTime, MaxForce, MaxSpeed, ArriveDist, Mass, Seek,
                                      TargetTransform.position.x, TargetTransform.position.y, TargetTransform.position.z, VisionRadius,
                                      Separate, Align, Cohese});
        computeShader.SetBuffer(_kernel, "cBuffer", cBuffer);

        // set constant int buffers
        cIntBuffer.SetData(new int[] { group_size_x, N });
        computeShader.SetBuffer(_kernel, "cIntBuffer", cIntBuffer);
        
        // set path buffer
        pathBuffer.SetData(Path.Select(x => new float[] { x.position.x, x.position.y, x.position.z }).SelectMany(x => x).ToArray());
        computeShader.SetBuffer(_kernel, "pathBuffer", pathBuffer);

        //alternate between reading and writing to position/velocityBufferA and position/velocityBufferB
        computeShader.SetBuffer(_kernel, "lastPositions", positionBuffers_GPU[i_last]);
        computeShader.SetBuffer(_kernel, "currentPositions", positionBuffers_GPU[i_current]);
        computeShader.SetBuffer(_kernel, "lastVelocities", velocityBuffers_GPU[i_last]);
        computeShader.SetBuffer(_kernel, "currentVelocities", velocityBuffers_GPU[i_current]);

        //dispatch
        computeShader.Dispatch(_kernel, group_size_x, group_size_y, group_size_z);
    }
    
    void Simulate_CPU()
    {
        // The positions that are drawn are positionBuffers_GPU[i_current]. This buffer
        // is the buffer that should be filled

        for(int idx = 0, i = 0; idx < N; ++idx, i+=3)
        {
            float dt = Time.deltaTime;
            Vector3 pos = positionBuffers_CPU[i_last, idx];
            Vector3 vel = velocityBuffers_CPU[i_last, idx];
            Vector3 force = Vector3.zero;

            //steering
            Vector3 vpath = pathfollowing(idx, 3, 1.5f, 2.0f, 3.0f);
            Vector3 vsteer = steering(idx); //separate, align, cohese
            //Vector3 vseek = seek(TargetTransform.position, pos);

            //apply all forces, clamp
            force += vpath * Seek;
            //force += vseek * Seek;
            force += vsteer;

            force = Vector3.ClampMagnitude(force, MaxForce);
            vel += force;

            //add vel
            vel = Vector3.ClampMagnitude(vel, MaxSpeed);
            pos += vel * dt;

            //store new position
            positionBuffers_CPU[i_current, idx] = pos;
            positionarray[i + 0] = pos.x;
            positionarray[i + 1] = pos.y;
            positionarray[i + 2] = pos.z;

            //store new velocity
            velocityBuffers_CPU[i_current, idx] = vel;
        }

        positionBuffers_GPU[i_current].SetData(positionarray);
        //velocityBuffers_GPU[i_current].SetData(velocities);
    }

    Vector3 steering(int idx)
    {
        Vector3 separateSum = Vector3.zero, alignSum = Vector3.zero, coheseSum = Vector3.zero;
        Vector3 separateSteer = Vector3.zero, alignSteer = Vector3.zero, coheseSteer = Vector3.zero;

        int count = 0;
        for (int i = 0; i < N; ++i)
        {
            Vector3 diff = positionBuffers_CPU[i_last, i] - positionBuffers_CPU[i_last, idx];
            float d = diff.magnitude;
            if(d > 0 && d < VisionRadius)
            {
                separateSum += diff / d;
                alignSum    += velocityBuffers_CPU[i_last, i];
                coheseSum   += positionBuffers_CPU[i_last, i];
                ++count;
            }
        }

        if(count > 0)
        {
            separateSum /= count;
            alignSum    /= count;
            coheseSum   /= count;

            Vector3.Normalize(separateSum);
            Vector3.Normalize(alignSum);

            separateSum *= MaxSpeed;
            alignSum    *= MaxSpeed;

            separateSteer   = velocityBuffers_CPU[i_last, idx] - separateSum;
            alignSteer      = alignSum - velocityBuffers_CPU[i_last, idx];
            coheseSteer     = seek(coheseSum, positionBuffers_CPU[i_last, idx]);
        }

        separateSteer   *= Separate;
        alignSteer      *= Align;
        coheseSteer     *= Cohese;

        return separateSteer + alignSteer + coheseSteer;
    }

    Vector3 seek(Vector3 target, Vector3 position)
    {
        Vector3 desired = target - position;
        float d = desired.magnitude;
        
        if (d < 0.0001) return Vector3.zero;
        
        return desired/d;
    }

    Vector3 pathfollowing(int idx, int pathlength, float R, float predict, float targetahead)
    {
        Vector3 pos = positionBuffers_CPU[i_last, idx];
        Vector3 vel = velocityBuffers_CPU[i_last, idx];
        Vector3 predictPos = pos + vel.normalized * predict;

        bool foundtarget = false;
        Vector3 besttarget = Vector3.zero;
        float bestdist = 100000.0f;
        for(int i = 0; i < pathlength - 1; ++i)
        {
            Vector3 a = Path[i].position;
            Vector3 b = Path[i+1].position;

            Vector3 target;
            Vector3 ab = b - a;
            float abLen = ab.magnitude;
            float t = Vector3.Dot(pos - a, ab) / ab.sqrMagnitude;
            bool lastsegment = (i + 1) == pathlength - 1;
            if(t < 0) //before path segment
            {
                target = a;
            }
            else if( t > 1)  //(abLen-2.5f*R)/abLen
            {
                if (!lastsegment) continue;        
                target = b;
            }
            else
            {
                target = a + ab*t;
            }

            //better target?
            float dist = Vector3.Distance(target, pos);
            if(dist < bestdist)
            {
                //look ahead
                float k = targetahead/ab.magnitude;
                if(lastsegment && t+k > 1) k = 0;           
                target += ab * k;

                //update target
                besttarget = target;
                bestdist = dist;
                foundtarget = true;
            }
        }

        //seek target point
        if(foundtarget && bestdist > R)
        {
            return seek(besttarget, pos);
        }

        return Vector3.zero;
    }
}